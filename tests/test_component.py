from troposphere import Ref
from troposphere.acmpca import Certificate

from henry.cfn_stack import CloudformationStack, load_settings


def test_build_a_component():
    data = {
        "Env.Dev": {
            ".Certificate": {
                "SigningAlgorithm": "SHA256WITHECDSA",
            },
        },
        "Resource": {
            "TemplateArn": "12345"
        },
        ".Certificate": {
            "SigningAlgorithm": "SHA256WITHRSA",
            "CertificateSigningRequest": "someotherstring",
            "Validity": {"Type": "ABSOLUTE", "Value": 1},
            "outputs": [
                {"name": "CertificateName"}
            ]
        },
    }
    settings = load_settings(data)
    stack = CloudformationStack("mystack", settings)
    c = stack.add_component(Certificate, "MyCert", CertificateSigningRequest="somestring")

    assert c.Validity.Type == "ABSOLUTE"
    assert c.SigningAlgorithm == "SHA256WITHRSA"
    assert c.CertificateSigningRequest == "somestring"
    assert c.TemplateArn == "12345"
    assert stack.template.outputs["CertificateName"].Value == Ref(c)
    assert stack.template.outputs["CertificateName"].Export.data == {"Name": "mystack-CertificateName"}

    with settings.context("Env.Dev"):
        stack = CloudformationStack("mystack", settings)
        c = stack.add_component(Certificate, "MyCert")
        assert c.Validity.Type == "ABSOLUTE"
        assert c.SigningAlgorithm == "SHA256WITHECDSA"
        assert c.CertificateSigningRequest == "someotherstring"
