from settingscascade import SettingsManager, ElementSchema
from troposphere import Template, AWSObject, Output, GetAtt, Ref, Export


class Env(ElementSchema):
    _name_ = "Env"
    _allowextra_ = True


class Resource(ElementSchema):
    _name_ = "Resource"
    _allowextra_ = True


def load_settings(*data):
    return SettingsManager(list(data), [Env, Resource])


class CloudformationStack:
    def __init__(self, stack_name: str, settings: SettingsManager):
        self.template = Template()
        self.settings = settings
        self.stack_name = stack_name

    def get_settings(self, aws_object_type, kwargs):
        data = {}
        for prop_name in aws_object_type.props:
            try:
                data[prop_name] = getattr(self.settings, prop_name)
            except ValueError:
                continue
        return dict(data, **kwargs)

    def add_component(self, aws_object_type, name, **kwargs):
        with self.settings.context(f"Resource.{aws_object_type.__name__}#{name}"):
            resource = aws_object_type.from_dict(
                name, self.get_settings(aws_object_type, kwargs)
            )
            self.template.add_resource(resource)
            self.add_outputs(resource)
        return resource

    def add_outputs(self, resource: AWSObject):
        try:
            outputs = self.settings.outputs
        except ValueError:
            return
        for output in outputs:
            name = output["name"]
            self.template.add_output(Output(
                name,
                Description=output.get("description", name),
                Value=GetAtt(resource, output["attribute"]) if output.get("attribute") else Ref(resource),
                Export=Export(f"{self.stack_name}-{name}")
            ))
