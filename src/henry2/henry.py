from importlib import import_module
from inspect import isclass
from pathlib import Path

from settingscascade import SettingsManager, ElementSchema
from yaml import safe_load
from troposphere import Template, AWSProperty


class Stack(ElementSchema):
    resources: list


base_path = Path(__file__).parent.joinpath("default_configs")

data = [safe_load(p.read_text()) for p in base_path.iterdir()]
data.append(safe_load(base_path.parent.joinpath("example.yml").read_text()))


class Henry:
    def __init__(self):
        self.config = SettingsManager(data, [Stack])

    def build_template(self, stack_name):
        resources = self.config.stack(name=stack_name).resources
        template = Template()
        with self.config.context(f".{stack_name}"):
            for resource in resources:
                with self.config.context(f".{resource}"):
                    template.add_resource(self.get_resource(*resource.split("#", 1)))
        return template

    def get_resource(self, resource_key, resource_id=""):
        module_name, cls_name = resource_key.split(".")
        module = import_module(f"troposphere.{module_name}".lower())
        cls = getattr(module, cls_name)
        return self.make_obj(module, cls, resource_id)

    def make_obj(self, module, obj_cls, resource_id):
        kwargs = {}
        for prop_name, (prop_type, required) in obj_cls.props.items():
            if isclass(prop_type) and issubclass(prop_type, AWSProperty):
                context = f".{prop_type.__name__}"
                if resource_id:
                    context += f"#{resource_id}"
                with self.config.context(context):
                    try:
                        kwargs[prop_name] = self.make_obj(module, prop_type, resource_id)
                    except ValueError:
                        if not required:
                            continue
                        raise
            elif isinstance(prop_type, list) and isclass(prop_type[0]) and issubclass(prop_type[0], AWSProperty):
                objs = []
                try:
                    val = getattr(self.config, prop_name)
                    if val is None:
                        raise ValueError()
                except ValueError:
                    if not required:
                        continue
                    raise
                for obj_name in val:
                    context = f".{prop_type[0].__name__}"
                    context += f"#{obj_name}"
                    with self.config.context(context):
                        objs.append(self.make_obj(module, prop_type[0], resource_id))
                kwargs[prop_name] = objs
            else:
                try:
                    val = getattr(self.config, prop_name)
                    if val is None:
                        raise ValueError()
                    kwargs[prop_name] = val
                except ValueError:
                    if not required:
                        continue
                    raise

        try:
            resource = obj_cls(f"{obj_cls.__name__}{resource_id}", **kwargs)
        except ValueError:
            print("Failed validation-")
            print(f"Class: {obj_cls}")
            print(f"kwargs: {kwargs}")
            raise
        return resource


h = Henry().build_template("pipeline")
print(h.to_yaml())

from troposphere.codepipeline import Pipeline