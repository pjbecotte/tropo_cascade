typing:
	mypy src/henry2 --ignore-missing-imports

format:
	isort -rc -tc src tests
	black .

check-format:
	isort -rc -tc --check-only src tests
	black --check .

lint:
	pylint src/settingscascade tests

unit-test:
	pytest --no-cov

test: lint typing check-format
	pytest

docker-test:
	docker build -t temp --build-arg PYTHON_VERSION=3.7 .
	docker run --rm temp make test

check: format test
