ARG PYTHON_VERSION=3.7
FROM python:${PYTHON_VERSION}-alpine

WORKDIR /app

RUN apk add --update make git gcc build-base
RUN pip install poetry==1.0.0a4 rye
RUN mkdir -p ~/.config/pypoetry
RUN poetry config settings.virtualenvs.create false
COPY pyproject.toml poetry.lock ./
RUN rye build-envs

COPY . ./
